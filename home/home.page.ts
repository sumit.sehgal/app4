import { Component } from '@angular/core';
import { MyserviceService } from '../myservice.service';

import {FormGroup, FormControl, Validators, NgForm } from "@angular/forms";
@Component({
  selector: 'app-home',
  template:`<ion-header>
  <ion-toolbar>
  <ion-title>FILE UPLOAD FC</ion-title>
  </ion-toolbar>
  </ion-header>
  <ion-content>
  <ion-card>
  <ion-card-content>
  <form (Submit)="onUpload(postsForm)" #postsForm="ngForm">
       <p>Fill the form to add a new post.</p>  
    <label for="title">Title:</label>
    <input type="text" name="title">
  
    <label for="content">Content:</label>
    <textarea name="content"></textarea>

    <label for="cover" class="cover"><i class="fas fa-upload"></i> Choose a file</label>
    <input type="file" name="cover" id="cover" (change)="display($event)">
  
    <input type="submit" [disabled]="!postsForm.valid" value="Submit">
  </form>
  </ion-card-content></ion-card>
  </ion-content>
  `,
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  selectedfile:File=null;
  constructor(private service:MyserviceService) {}
display($event: Event)
{
    this.selectedfile = $event.target["files"];
    console.log(this.selectedfile);
}
  onUpload(data: FormData)
  {
  this.service.createpost(data,this.selectedfile);    

  }
}
